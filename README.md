# raduchiriac TA10 project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains a series of tests ran on several features of the demo website  https://parabank.parasoft.com/parabank/index.htm , utilising Cucumber framework. 

The project includes page object models, Cucumber feature files with various scenarios and runners for the features we approached, saving the test reports in collections on https://reports.cucumber.io/


[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
