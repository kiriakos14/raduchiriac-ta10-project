package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageobjectmodel.RegisterPage;
import java.util.Random;
import static stepdefinitions.Hooks.driver;

public class RegisterStepDefinitions {
    RegisterPage registerPage;

    public String stringGenerator() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    private String newaccountFirstName = stringGenerator();
    private String newaccountLastName = stringGenerator();
    private String newaccountTestAddress = stringGenerator();
    private String newaccountTestCity = stringGenerator();
    private String newaccountTestState = stringGenerator();
    private String newaccountTestZip = stringGenerator();
    private String newaccountTestSSN = stringGenerator();
    private String newaccountTestPhone = stringGenerator();
    private String newaccountUser = stringGenerator();
    private String newaccountPass = stringGenerator();


    @Given("Be on Register page")
    public void beOnRegisterPage() {
        registerPage = new RegisterPage(driver);
    }

    @When("Fill in First Name with {string}")
    public void fillInFirstNameWith(String firstname) {
        registerPage.fillFirstName(firstname);
    }

    @And("Fill in Last Name with {string}")
    public void fillInLastNameWith(String lastname) {
        registerPage.fillLastName(lastname);
    }

    @And("Fill in Address with {string}")
    public void fillInAddressWith(String address) {
        registerPage.fillAddress(address);
    }

    @And("Fill in City with {string}")
    public void fillInCityWith(String city) {
        registerPage.fillCity(city);
    }

    @And("Fill in State with {string}")
    public void fillInStateWith(String state) {
        registerPage.fillState(state);
    }

    @And("Fill in Zip Code with {string}")
    public void fillInZipCodeWith(String zipcode) {
        registerPage.fillZipcode(zipcode);
    }

    @And("Fill in Phone # with {string}")
    public void fillInPhoneWith(String phone) {
        registerPage.fillPhonenumber(phone);
    }

    @And("Fill in SSN with {string}")
    public void fillInSSNWith(String ssn) {
        registerPage.fillSSN(ssn);
    }

    @And("Fill in Username with {string}")
    public void fillInUsernameWith(String username) {
        registerPage.fillUsername(username);
    }

    @And("Fill in Password with {string}")
    public void fillInPassword(String password) {
        registerPage.fillPassword(password);
    }

    @And("Reconfirm Password with {string}")
    public void fillInConfirmation(String password) {
        registerPage.confirmPassword(password);
    }

    @And("Click on Register button")
    public void clickOnRegisterButton() {
        registerPage.clickOnRegisterButton();
    }

    @Then("Confirmation message is displayed and is correct")
    public void confirmationMessageIsDisplayed() {
        Assert.assertTrue("The welcoming message is not displayed as expected",
                driver.findElement(By.cssSelector("#rightPanel")).isDisplayed());
        System.out.println(driver.findElement(By.cssSelector("#rightPanel>h1")).getText());
        Assert.assertTrue("The displayed message is not the welcoming message",
                driver.findElement(By.cssSelector("#rightPanel>h1")).getText().startsWith("Welcome"));
    }

    @Then("Error message is displayed - username already exists")
    public void errorMessageIsDisplayed() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.username.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "This username already exists.", errorMessage.getText());
    }

    @Then("Error message is displayed - first name is required")
    public void errorMessageIsDisplayedFirstNameIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.firstName.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "First name is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - last name is required")
    public void errorMessageIsDisplayedLastNameIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.lastName.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "Last name is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - address name is required")
    public void errorMessageIsDisplayedAddressNameIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.address.street.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "Address is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - city name is required")
    public void errorMessageIsDisplayedCityNameIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.address.city.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "City is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - state name is required")
    public void errorMessageIsDisplayedStateNameIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.address.state.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "State is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - Zip Code is required")
    public void errorMessageIsDisplayedZipcodeIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.address.zipCode.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "Zip Code is required.", errorMessage.getText());
    }


    @Then("Error message is displayed - Social Security Number is required")
    public void errorMessageIsDisplayedSSNIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.ssn.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "Social Security Number is required.", errorMessage.getText());

    }

    @Then("Error message is displayed - Username is required")
    public void errorMessageIsDisplayedUsernameIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.username.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "Username is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - Password is required")
    public void errorMessageIsDisplayedPasswordIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='customer.password.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one", "Password is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - Password confirmation is required")

    public void errorMessageIsDisplayedPasswordConfirmationIsRequired() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='repeatedPassword.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one",
                "Password confirmation is required.", errorMessage.getText());
    }

    @Then("Error message is displayed - Password did not match")
    public void errorMessageIsDisplayedPasswordDidNotMatch() {
        WebElement errorMessage = driver.findElement(By.cssSelector("[id='repeatedPassword.errors']"));
        Assert.assertTrue("The error message is not displayed as expected", errorMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one",
                "Passwords did not match.", errorMessage.getText());
    }

    @When("Fill in all required details")
    public void fillInAllRequiredDetails() {

        registerPage.fillFirstName(newaccountFirstName);
        registerPage.fillLastName(newaccountLastName);
        registerPage.fillAddress(newaccountTestAddress);
        registerPage.fillCity(newaccountTestCity);
        registerPage.fillState(newaccountTestState);
        registerPage.fillZipcode(newaccountTestZip);
        registerPage.fillSSN(newaccountTestSSN);
        registerPage.fillPhonenumber(newaccountTestPhone);
        registerPage.fillUsername(newaccountUser);
        registerPage.fillPassword(newaccountPass);
        registerPage.confirmPassword(newaccountPass);
    }

    @Given("New user registers with required fields")
    public void registerWithRequiredFields() {
        registerPage = new RegisterPage(driver);
        registerPage.fillFirstName(newaccountFirstName);
        registerPage.fillLastName(newaccountLastName);
        registerPage.fillAddress(newaccountTestAddress);
        registerPage.fillCity(newaccountTestCity);
        registerPage.fillState(newaccountTestState);
        registerPage.fillZipcode(newaccountTestZip);
        registerPage.fillPhonenumber(newaccountTestPhone);
        registerPage.fillSSN(newaccountTestSSN);
        registerPage.fillUsername(newaccountUser);
        registerPage.fillPassword(newaccountPass);
        registerPage.confirmPassword(newaccountPass);
        registerPage.clickOnRegisterButton();
    }


}
