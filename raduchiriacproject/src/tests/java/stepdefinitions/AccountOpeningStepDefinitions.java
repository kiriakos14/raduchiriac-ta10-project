package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjectmodel.OpenNewAccountPage;
import pageobjectmodel.RegisterPage;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static stepdefinitions.Hooks.driver;

public class AccountOpeningStepDefinitions {
    OpenNewAccountPage accountOpeningPage= new OpenNewAccountPage(driver);

    @When("clicking on Open New Account link")
    public void clickingOnOpenNewAccountLink() {
        accountOpeningPage.clickOnOpenNewAccountLink();
    }

    @And("selecting Checking account from the dropdown menu")
    public void selectingCheckingAccountFromTheDropdownMenu() {
        accountOpeningPage.selectCheckingAccount();
    }

    @And("selecting Savings account from the dropdown menu")
    public void selectingSavingsAccountFromTheDropdownMenu() { accountOpeningPage.selectSavingsAccount();
    }

    @And("clicking on Open New Account button")
    public void clickingOnOpenNewAccountButton() {
        accountOpeningPage.clickOnOpenNewAccountButton();
    }

//    @And("selecting a preexisting account to transfer funds from")
//    public void selectingPreexistingAccountToTransferFundsFrom() {
//        accountOpeningPage.selectPreExistingAccount();
//    }

    @Then("{string} message is displayed")
    public void messageIsDisplayed(String message) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("[ng-if='showResult'] [class='title']"))));
        WebElement confirmationMessage = driver.findElement(By.cssSelector("[ng-if='showResult'] [class='title']"));
        Assert.assertTrue("The confirmation message is not displayed as expected", confirmationMessage.isDisplayed());
        Assert.assertEquals("The message is not the expected one",
                message, confirmationMessage.getText());
    }

    @And("the new account appears on the Accounts overview")
    public void theNewAccountAppearsOnTheAccountsOverview() {
        String newAccountNo = driver.findElement(By.cssSelector("[id='newAccountId']")).getText();
        driver.findElement(By.cssSelector("[href= '/parabank/overview.htm']")).click();
        List<WebElement> accounts = driver.findElements(By.cssSelector("[id='accountTable'] [href]"));
        int accConfirmationSwitch = 0;
        for (WebElement account: accounts) {
            if (account.getText().equals(newAccountNo)){
                accConfirmationSwitch=1;
            }
        }
        Assert.assertEquals("The account does not appear as expected", 1, accConfirmationSwitch);
    }
}
