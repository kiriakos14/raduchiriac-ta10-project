@Register_Negative
Feature: Register negative scenarios

  Scenario: Register with all required fields except first name
    Given Be on Register page
    When Fill in First Name with ""
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "username"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - first name is required

  Scenario: Register with all required fields except last name
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with ""
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "username"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - last name is required

  Scenario: Register with all required fields except address
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with ""
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "username"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - address name is required

  Scenario: Register with all required fields except city
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with ""
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "username"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - city name is required

  Scenario: Register with all required fields except state
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with ""
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "username"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - state name is required

  Scenario: Register with all required fields except zipcode
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with ""
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "username"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - Zip Code is required

  Scenario: Register with all required fields except SSN
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with ""
    And Fill in Username with "username_noSSN"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - Social Security Number is required

  Scenario: Register with all required fields except Username
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "SSN2"
    And Fill in Username with ""
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button

    Then Error message is displayed - Username is required

  Scenario: Register with all required fields except Password and Confirm
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "SSN2"
    And Fill in Username with "username_nopassword_noconfirmation"
    And Fill in Password with ""
    And Reconfirm Password with ""
    And Click on Register button

    Then Error message is displayed - Password is required
    And Error message is displayed - Password confirmation is required

  Scenario: Register with all required fields except password confirmation
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "SSN2"
    And Fill in Username with "username_noconfirmation"
    And Fill in Password with "password_noconfirmation"
    And Reconfirm Password with ""
    And Click on Register button

    Then Error message is displayed - Password confirmation is required

  Scenario: Register with all required fields except Password
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "SSN2"
    And Fill in Username with "username_nopassword"
    And Fill in Password with ""
    And Reconfirm Password with "password"
    And Click on Register button

    Then Error message is displayed - Password is required

  Scenario: Register with all required fields but Password confirmation different from Password
    Given Be on Register page
    When Fill in First Name with "firstname3"
    And Fill in Last Name with "lastname3"
    And Fill in Address with "address3"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "SSN2"
    And Fill in Username with "username_wrong_pass_confirmation"
    And Fill in Password with "password"
    And Reconfirm Password with "password1"
    And Click on Register button

    Then Error message is displayed - Password did not match


  Scenario: Register with all required fields but preexistent username
    Given Be on Register page
    When Fill in First Name with "firstname2"
    And Fill in Last Name with "lastname2"
    And Fill in Address with "address2"
    And Fill in City with "city2"
    And Fill in State with "state2"
    And Fill in Zip Code with "zipcode2"
    And Fill in Phone # with "phoneNumber2"
    And Fill in SSN with "securityNumber2"
    And Fill in Username with "marcelinho423"
    And Fill in Password with "password2"
    And Reconfirm Password with "password2"
    And Click on Register button
    And Be on Register page
    And Fill in First Name with "firstname22"
    And Fill in Last Name with "lastname22"
    And Fill in Address with "address22"
    And Fill in City with "city22"
    And Fill in State with "state22"
    And Fill in Zip Code with "zipcode22"
    And Fill in Phone # with "phoneNumber22"
    And Fill in SSN with "securityNumber22"
    And Fill in Username with "marcelinho423"
    And Fill in Password with "password22"
    And Reconfirm Password with "password22"
    And Click on Register button

    Then Error message is displayed - username already exists