@OpenAccount
Feature: open new account for existing user


  Scenario: open new checking account for existing user


    Given New user registers with required fields
    When clicking on Open New Account link
    And selecting Checking account from the dropdown menu
    And clicking on Open New Account button
    Then "Account Opened!" message is displayed
    And the new account appears on the Accounts overview


  Scenario: open new savings account for existing user

    Given New user registers with required fields
    When clicking on Open New Account link
    And selecting Savings account from the dropdown menu
    And clicking on Open New Account button
    Then "Account Opened!" message is displayed
    And the new account appears on the Accounts overview
