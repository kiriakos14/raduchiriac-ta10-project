@Register_Positive
Feature: Register positive scenario

  Scenario Outline: Register with required fields from table
    Given Be on Register page
    When Fill in First Name with "<firstname>"
    And Fill in Last Name with "<lastname>"
    And Fill in Address with "<address>"
    And Fill in City with "<city>"
    And Fill in State with "<state>"
    And Fill in Zip Code with "<zipcode>"
    And Fill in Phone # with "<phoneNumber>"
    And Fill in SSN with "<securityNumber>"
    And Fill in Username with "<username>"
    And Fill in Password with "<password>"
    And Reconfirm Password with "<password>"
    And Click on Register button

    Then Confirmation message is displayed and is correct

    Examples:
      | firstname | lastname   | address            | city      | state   | zipcode | phoneNumber | securityNumber | username     | password |
      | georgel   | georgescu  | Str Georgiei nr. 5 | Georgesti | Georgia | 700125  | 0742061867  | 1234567        | georgelinho1 | passw1   |
      | fanel     | fanescu    | Str Faniei nr. 5   | Fanesti   | Fania   | 700126  | 0742061868  | 1234568        | fanelinho1 | passw2   |
      | sandel    | sandescu   | Str Sandei nr. 2   | Sandesti  | Sanda   | 700125  |             | 1234545        | sandelinho1  | passw3   |
      | marcel    | marcelescu | Str Marciei nr. 8  | Marcesti  | Marca   | 700987  | 0786543556  | 42563667       | marcelinho1 | passw123 |


  Scenario: Register with required fields from variables
    Given Be on Register page
    When Fill in all required details
    And Click on Register button

    Then Confirmation message is displayed and is correct


