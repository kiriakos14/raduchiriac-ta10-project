package runners;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;


@RunWith(Cucumber.class)
@CucumberOptions(
        features= "raduchiriacproject/src/tests/java/features/register-negative.feature",
        glue= "stepdefinitions",
        tags="@Register_Negative",
        plugin="pretty",
        publish=true,
        //publish generates the report as a link in the console which can thereafter be shared
        snippets = CAMELCASE
)


public class RegisterNegFeaturesRunners {
}
