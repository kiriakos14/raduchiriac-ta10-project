package runners;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "raduchiriacproject/src/tests/java/features/open new account.feature",
        glue = "stepdefinitions",
        tags = "@OpenAccount",
        plugin = "pretty",
        publish = true,
        snippets = CAMELCASE
)


public class OpenNewAccountFeaturesRunners {
}
