package pageobjectmodel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class OpenNewAccountPage {
    private WebDriver driver;
//    public String url = "https://parabank.parasoft.com/parabank/openaccount.htm";

    By openNewAccountLink = By.cssSelector("[href='/parabank/openaccount.htm']");
    By currentPageTitle = By.cssSelector("[class=title]");
    By accountTypeDropdown = By.cssSelector("#type");

    public OpenNewAccountPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnOpenNewAccountLink() {
        driver.findElement(openNewAccountLink).click();
    }

    public void clickOnOpenNewAccountButton() {
        WebElement openNewAccountButton = driver.findElement(By.cssSelector("[type='submit']"));
        WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        explicitWait.until(ExpectedConditions.visibilityOf(openNewAccountButton));
        WebElement currentPage = driver.findElement(currentPageTitle);
        System.out.println("The button name is:" + openNewAccountButton.getAttribute("value"));
        System.out.println("Is the button displayed? " + openNewAccountButton.isDisplayed());
        System.out.println("Is the button enabled? " + openNewAccountButton.isEnabled());
        int numberOfAttempts = 1;
        openNewAccountButton.click();

// the following part of the method code is solely to deal with a recurring fault in the website:
// - the open new acc button does not always function on the first click, so several clicks will be made if
//        the webpage does not change as expected

        while ((numberOfAttempts < 3) && (currentPage.isDisplayed()) && (currentPage.getText().equals("Open New Account"))) {
            System.out.println("We are still on the page:" + currentPage.getText());
            numberOfAttempts++;
            System.out.println("Attempting click no: " + numberOfAttempts);
            openNewAccountButton.click();
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("[ng-if='showResult'] [class='title']"))));
            if (driver.findElement(By.cssSelector("[ng-if='showResult'] [class='title']")).isDisplayed()) {
                numberOfAttempts = 3;
            }
        }
    }

    public void selectCheckingAccount() {
        Select accountTypeDropDown = new Select(driver.findElement(accountTypeDropdown));
        accountTypeDropDown.selectByValue("0");
    }

    public void selectSavingsAccount() {
        Select accountTypeDropDown = new Select(driver.findElement(accountTypeDropdown));
        accountTypeDropDown.selectByValue("1");
    }
}
