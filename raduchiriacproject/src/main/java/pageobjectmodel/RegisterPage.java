package pageobjectmodel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {

    private WebDriver driver;
    public String url = "https://parabank.parasoft.com/parabank/register.htm";

    By firstNameField = By.cssSelector("[id='customer.firstName']");
    By lastNameField = By.cssSelector("[id='customer.lastName']");
    By streetAddressField = By.cssSelector("[id='customer.address.street']");
    By cityField = By.cssSelector("[id='customer.address.city']");
    By stateField = By.cssSelector("[id='customer.address.state']");
    By zipCodeField = By.cssSelector("[id='customer.address.zipCode']");
    By phoneNumberField = By.cssSelector("[id='customer.phoneNumber']");
    By socialSecurityNoField = By.cssSelector("[id='customer.ssn']");
    By usernameField = By.cssSelector("[id='customer.username']");
    By passwordField = By.cssSelector("[id='customer.password']");
    By passwordConfirmationField = By.cssSelector("[id='repeatedPassword']");
    By registerButton = By.cssSelector("input[value='Register']");


    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        this.driver.get(url);
    }

    public void fillFirstName(String firstname) {
//      - direct approach if not using the By variables defined above:
//        driver.findElement(By.cssSelector("[id='customer.firstName']")).sendKeys(firstname);
        driver.findElement(firstNameField).sendKeys(firstname);
    }

    public void fillLastName(String lastname) {
//        driver.findElement(By.cssSelector("[id='customer.lastName']")).sendKeys(lastname);
        driver.findElement(lastNameField).sendKeys(lastname);
    }

    public void fillAddress(String address) {
//        driver.findElement(By.cssSelector("[id='customer.address.street']")).sendKeys(address);
        driver.findElement(streetAddressField).sendKeys(address);
    }

    public void fillCity(String city) {
//        driver.findElement(By.cssSelector("[id='customer.address.city']")).sendKeys(city);
        driver.findElement(cityField).sendKeys(city);
    }


    public void fillState(String state) {
//        driver.findElement(By.cssSelector("[id='customer.address.state']")).sendKeys(state);
        driver.findElement(stateField).sendKeys(state);
    }

    public void fillZipcode(String zipcode) {
//        driver.findElement(By.cssSelector("[id='customer.address.zipCode']")).sendKeys(zipcode);
        driver.findElement(zipCodeField).sendKeys(zipcode);
    }

    public void fillPhonenumber(String phone) {
//        driver.findElement(By.cssSelector("[id='customer.phoneNumber']")).sendKeys(phone);
        driver.findElement(phoneNumberField).sendKeys(phone);
    }

    public void fillSSN(String ssn) {
//        driver.findElement(By.cssSelector("[id='customer.ssn']")).sendKeys(ssn);
        driver.findElement(socialSecurityNoField).sendKeys(ssn);
    }


    public void fillUsername(String username) {
//        driver.findElement(By.cssSelector("[id='customer.username']")).sendKeys(username);
        driver.findElement(usernameField).sendKeys(username);
    }

    public void fillPassword(String password) {
//        driver.findElement(By.cssSelector("[id='customer.password']")).sendKeys(password);
        driver.findElement(passwordField).sendKeys(password);
    }

    public void confirmPassword(String password) {
//        driver.findElement(By.cssSelector("[id='repeatedPassword']")).sendKeys(password);
        driver.findElement(passwordConfirmationField).sendKeys(password);
    }

    public void clickOnRegisterButton() {
//        driver.findElement(By.cssSelector("input[value='Register']")).click();
        driver.findElement(registerButton).click();
    }
}
